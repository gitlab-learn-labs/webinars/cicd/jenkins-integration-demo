# Demo Overview
While GitLab provides its own CI/CD functionality, there are specific cases where customers who move to GitLab for SCM might want to integrate with Jenkins before migrating to GitLab CI:
  - complex legacy build jobs might take time to migrate to GitLab
  - teams are comfortable with Jenkins CI but want to take advantage of as many GitLab features as possible
  - a mix of teams are using different tools for CI/CD, some Jenkins and some GitLab, but want consistent CI/CD analytics reporting across all teams

This demo shows how to configure the [GitLab Jenkins Integration](https://docs.gitlab.com/ee/integration/jenkins.html) with a Jenkins Pipeline project to kick off external Jenkins jobs in parallel with GitLab CI jobs. 

The demo project runs GitLab Secret Detection, SAST, and Dependency scanning jobs, as well as two placeholder build and test jobs on an external Jenkins server (it is recommended to use the Jenkins instance under the [Demo Engineering shared infrastructure](https://gitlab.com/gitlab-learn-labs/webinars/demo-engineering-shared-infra)). Completion statuses for the Jenkins jobs are reported back to GitLab and visible in the GitLab pipeline view, as well as Merge Requests 

Please see this [slide deck](https://docs.google.com/presentation/d/1BKFZjbVngKnX7lhePwaxCe8QQxjojJwcfpDeSaQSBz8/) for the full presentation.

# Demo Configuration

## Configure Jenkins Integration With GitLab

- Configure GitLab project
  - Create a fork of this project
  - Setup a project access token for use by Jenkins
    - **Settings** -> **Access Tokens**
    - Select API scope with Developer role
    - Copy token to a secure location

## Configure Jenkins Instance
  - Install GitLab and Git plugins
  - Configure GitLab Plugin
    - **Manage Jenkins** -> **Configure System**
    - In the GitLab section, select **Enable authentication for '/project' end-point**
    - Add GitLab host url (ex: https://gitlab.com)
    - Setup credentials
      - Below connection configuration, click **Add -> Jenkins Credential Provider -> GitLab API Token**
      - Use GitLab project access token
      - Name the credential
      - Save and apply
    - Test connection
      - If success, Jenkins can access the GitLab Project

## Setup Jenkins Pipeline  project
  - In Jenkins dashboard, click **New Item -> Pipeline -> click ok**
  - In new Project, click **Configure**
  - Select **“use alternate credential”**, then choose from dropdown the previously created API token credential.
  - In Build Triggers, select **Build when Change pushed to GitLab** 
     - In **Enable Triggers**, select
        - Push
        - Open MR
        - Approved Merge Requests
        - Comments
        - Accepted MR Events
        - Closed MR Events
    - In **Advanced**, provide a Pending Build Name of <i>jenkins-job</i>
       - This creates a pending placeholder Jenkins job when the GitLab pipeline is kicked off
    - Leave the defaults selected for 
       - Enable CI Skip
       - Ignore WIP MRS
       - Set Build to build cause
       - Allow all branches
    - In Pipeline, select **Definition -> Pipeline Script from SCM**
       - Select  **SCM -> GitLab**, then
          - For Repo URL, use URL copied from the GitLab project via "Clone with HTTPS”
          - If using a Private GitLab project, you will need to setup another Jenkins credential with the same Project Access Token, except as a **“Username with Password”** credential in Jenkins
            - **Domain**: Global credentials
            - **Kind**: Username with Password
            - **Scope**: Global
            - **Username**: your gitlab username
            - **Password**: Project access token for GitLab project
        - If you do not see a 4xx HTTP error message in Jenkins, the configuration is successful
    - In Branches to Build, use <i>${gitlabSourceBranch}</i> as the **Branch Specifier**
      - This ensures Jenkins pulls the correct branch / commit after pulling in the repo
    - Select **Script Path -> Jenkins File**
      - Deselect **“Light Weight Checkout”**
      - Save and Apply

## Configure Jenkins Integration in GitLab
  - In the GitLab project, go to **Settings -> Integrations -> Jenkins**
    - Select Push, MR, and Tag Push for trigger
    - Enter Jenkins Server URL
    - Select Disable SSL verification if using custom certs in your environment
    - **Project name**: 
      - Copy from Jenkins project URL with URL escapes (ex: <i>/GitLab%20Jenkins%20Integration%20Pipeline%20</i>)
    - **Username**: <i>Jenkins username</i>
    - **Password**: <i>Jenkins password</i>
 - Test the connection. 
   - If there is HTTP 200 response, the integration is properly configrued

## Test Configuration
 * Create a commit in one of the existing feature branches (or create a new one)
 * Everything works as intended if the commmit creates a GitLab Pipeline with the test stage containing the security scan jobs and an external stage with a Jenkins Build and Test job.

# Java Spring Template 

The Java Spring portion of this project is based on a GitLab Project Template. Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).


# Contributing to this Demo
If you would like to contribute to this demo, please open an Issue and mention @rcain. Thank you to Cherry Han, Manjeet Singh, and Julie Byrne for providing materials that served as the foundation for this demo!

# Additional Resources
- [Jenkins Integration for GitLab Documentation](https://docs.gitlab.com/ee/integration/jenkins.html)
- [Jenkins to GitLab CI Migration Guide](https://docs.gitlab.com/ee/ci/migration/jenkins.html)
- [GitLab Plugin for Jenkins Documentation](https://plugins.jenkins.io/gitlab-plugin/)
- [JenkinsFile Wrapper for GitLab CI](https://docs.gitlab.com/ee/ci/migration/jenkins.html#jenkinsfile-wrapper)
  - The JenkinsFile wrapper helps users transition to GitLab CI without having to translate their JenkinsFile to a GitLab CI file. It achieves this by running the JenkinsFile in GitLab CI via containerized Jenkins environment. There might be limitations in terms of plugins used in Jenkins, maybe performance as well. The advantage here is that both build results and artifacts will be stored in GitLab.
  - As of mid-2023, further development of the JenkinsFile wrapper is backlogged. The following Epics can be tracked for future updates:
    - https://gitlab.com/groups/gitlab-org/-/epics/2779
    - https://gitlab.com/groups/gitlab-org/-/epics/2735

